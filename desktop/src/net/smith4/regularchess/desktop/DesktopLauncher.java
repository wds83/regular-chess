package net.smith4.regularchess.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.smith4.regularchess.RegularChessMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = RegularChessMain.WIDTH;
		config.height = RegularChessMain.HEIGHT;
		config.title = RegularChessMain.TITLE;
		new LwjglApplication(new RegularChessMain(), config);
	}
}
