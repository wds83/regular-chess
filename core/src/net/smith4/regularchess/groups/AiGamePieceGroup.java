package net.smith4.regularchess.groups;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

import net.smith4.regularchess.ai.PieceState;
import net.smith4.regularchess.ai.PlayState;
import net.smith4.regularchess.sprites.*;
import net.smith4.regularchess.sprites.AiGamePiece;
import net.smith4.regularchess.states.ChessboardState;


/**
 * Created by Warren on 10/29/2017.
 */

public abstract class AiGamePieceGroup extends Group {
    //protected Array<Vector3> moveLocations;
    protected Board board;
    protected AiGamePiece piece;
    protected Array<GamePieceGroup> gamePieces;
    protected Array<AiGamePieceGroup> aiGamePieces;
    protected StateMachine<AiGamePieceGroup, PieceState> stateMachine;
    protected GamePieceGroup.Weapon weapon;
    protected GamePieceGroup currentTarget;
    protected moveNode nextMove;
    protected int health;
    protected int might;
    protected int range;
    protected boolean isDead = false;
    protected StateMachine<ChessboardState, PlayState> boardStateMachine;

    public static class moveNode{
        Vector3 location;
        moveNode prevNode;
        int distance;
        public moveNode(Vector3 _location, moveNode parent, int dist){
            location = _location;
            prevNode = parent;
            distance = dist;
        }

    }

    protected Array<moveNode> moveGrid;
    protected Array<Vector3> attackGrid;

    public AiGamePieceGroup(Board _board, Array<GamePieceGroup> _gamePieces, Array<AiGamePieceGroup> _aiGamePieces, StateMachine<ChessboardState, PlayState> _boardStateMachine){
        super();
        board = _board;
        gamePieces = _gamePieces;
        aiGamePieces = _aiGamePieces;
        moveGrid = new Array<moveNode>();
        attackGrid = new Array<Vector3>();
        stateMachine = new DefaultStateMachine<AiGamePieceGroup, PieceState>(this, PieceState.SLEEP);
        boardStateMachine = _boardStateMachine;


    }

    protected abstract void generateMoveMap(moveNode node, int distance);

    protected boolean checkNodeExists(moveNode node){
        //check bounds
        if(node.location.x >= board.getSize().x || node.location.y >= board.getSize().y || node.location.x < 0 || node.location.y < 0){
            return true;
        }
        //check moveGrid
        for(int i = 0; i < moveGrid.size; i++){
            if(node.location.x == moveGrid.get(i).location.x && node.location.y == moveGrid.get(i).location.y){
                //remove less optimal move
                if(moveGrid.get(i).distance > node.distance){
                    moveGrid.removeIndex(i);
                    System.out.println("More Optimal move found");
                    return false;
                }
                return true;
            }

        }

//        //check enemies
//        for(int i = 0; i < gamePieces.size; i++){
//            float enemyX = gamePieces.get(i).getPiece().getLocation().x;
//            float enemyY = gamePieces.get(i).getPiece().getLocation().y;
//            if(node.location.x == enemyX && node.location.y == enemyY){
//                return true;
//            }
//        }
        //check allies
        for(int i = 0; i < aiGamePieces.size; i++){
            float allyX = aiGamePieces.get(i).getPiece().getLocation().x;
            float allyY = aiGamePieces.get(i).getPiece().getLocation().y;
            if(node.location.x == allyX && node.location.y == allyY){
                return true;
            }
        }
        return false;
    }

    public abstract void generateAttackMap(Vector3 location);

    public void setTarget(GamePieceGroup _target){currentTarget = _target;}
    public GamePieceGroup getTarget(){
        return currentTarget;
    }


    public void dealDamage(GamePieceGroup target){
        target.takeDamage(calculateDamage(target));
        System.out.printf("Target health: %d\n", target.getHealth());
    }
    public void attack(){
        System.out.println("Attacking...");
        stateMachine.changeState(PieceState.ATTACK);
    }

    public void move(){
        System.out.println("Moving...");
        stateMachine.changeState(PieceState.MOVE);
    }

    public boolean canAttack(GamePieceGroup target){

        if(target == null){
            return false;
        }
        generateAttackMap(piece.getLocation());
        for(Vector3 enemyPos:attackGrid){
            if(enemyPos.x == target.getPiece().getLocation().x && enemyPos.y == target.getPiece().getLocation().y){
                return true;
            }
        }
        return false;
    }

    public AiGamePiece getPiece(){
        return piece;
    }

    public int getHealth(){return health;}


    public void takeDamage(int value){
        health -= value;
        if(health <= 0){
            isDead = true;
            removePiece(this);
        }
    }
    public void removePiece(AiGamePieceGroup piece){
        for(int i = 0; i < aiGamePieces.size; i++){
            if(aiGamePieces.get(i).getPiece().getLocation().x == piece.getPiece().getLocation().x && aiGamePieces.get(i).getPiece().getLocation().y == piece.getPiece().getLocation().y){
                aiGamePieces.removeIndex(i);
                this.getParent().removeActor(this);
            }
        }
    }

    public int getMight(){return might;}

    public void backtraceMove(GamePieceGroup target){
        if(target == null){
            System.out.println("No target, sleeping....");
            sleep();
            nextMove = null;
            return;
        }
        moveNode currentNode = null;
        //find moveNode of target
        for(moveNode move: moveGrid){
            if(move.location.x == target.getPiece().getLocation().x && move.location.y == target.getPiece().getLocation().y){
                currentNode = move;
                break;
            }
        }
        if(currentNode == null){
            System.out.println("cannot find target movenode");
            nextMove = null;
            return;
        }
        //backtrace to next move
        while(currentNode.distance > 1 ){
            currentNode = currentNode.prevNode;
        }
        nextMove = currentNode;
        makeMove(currentNode.location);
        return;

    }

    protected void displayMove(){
        MoveTile moveActor = new MoveTile(board, (int)nextMove.location.x, (int)nextMove.location.y);
        moveActor.setTouchable(Touchable.disabled);
        //AiGamePiece moveActor = new AiGamePiece(board, "movement.png", (int)nextMove.location.x, (int)nextMove.location.y);
        moveActor.setName("moveActor");
        getParent().addActor(moveActor);
        childrenChanged();
    }

    protected void displayAttack(){
        for(Vector3 attackMove: attackGrid){
            if(attackMove.x == currentTarget.getPiece().getLocation().x && attackMove.y == currentTarget.getPiece().getLocation().y){
                MoveTile attackActor = new MoveTile(board, (int)currentTarget.getPiece().getLocation().x, (int)currentTarget.getPiece().getLocation().y);
                attackActor.setMoveType(MoveTile.MoveType.ATTACK);
                attackActor.setTouchable(Touchable.disabled);
                //AiGamePiece moveActor = new AiGamePiece(board, "movement.png", (int)nextMove.location.x, (int)nextMove.location.y);
                attackActor.setName("attackActor");
                getParent().addActor(attackActor);
                childrenChanged();
            }
        }

    }



    public void hideMove(String actorName){
        for(Actor actor:getParent().getChildren()){
            if((actor.getName()) == actorName){
                getParent().removeActor(actor);
                childrenChanged();
            }
        }
    }

//    public void setNextMove(moveNode move){
//        nextMove = move;
//    }
//
//    public moveNode getNextMove(){
//        return nextMove;
//    }

    public Board getBoard(){return board;}

    public void makeMove(Vector3 _moveLocation){
        //don't move over enemy pieces
        for(int i = 0; i < gamePieces.size; i++){
            float enemyX = gamePieces.get(i).getPiece().getLocation().x;
            float enemyY = gamePieces.get(i).getPiece().getLocation().y;
            if(_moveLocation.x == enemyX && _moveLocation.y == enemyY){
                return;
            }
        }
        displayMove();
        final Vector3 moveLocation = _moveLocation;
        float delay = 0.25f;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                hideMove("moveActor");
                if(moveLocation == null){
                    return;
                }

                piece.setMoved();//set first move to false for pawns
                piece.updatePosition(board.getLocation((int)moveLocation.x, (int)moveLocation.y));
                piece.updateLocation(moveLocation);
                //destroy all moves for all pieces
                for(int i = 0; i < gamePieces.size; i++){
                    gamePieces.get(i).destroyMoves();
                    gamePieces.get(i).movesGenerated = false;
                }
                for(int i = 0; i < aiGamePieces.size; i++){
                    aiGamePieces.get(i).moveGrid.clear();
                }
            }
        }, delay);

    }

    public void makeAttack(){
        generateAttackMap(piece.getLocation());
        if(canAttack(currentTarget)){
            displayAttack();
            dealDamage(currentTarget);
        }
        displayAttack();
        final AiGamePieceGroup currentEntity = this;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                hideMove("attackActor");
                currentEntity.sleep();
            }
        }, 0.25f);
    }

    public void printMoveMap(){
        for(moveNode move:moveGrid){
            System.out.printf("X=%f, Y=%f, distance=%d\n", move.location.x, move.location.y, move.distance);
        }
    }

    public GamePieceGroup findTarget(){
        if(gamePieces.size == 0){
            return null;
        }
        generateMoveMap(new moveNode(piece.getLocation(), null, 0), 0);
        GamePieceGroup target = null;
        int targetDist = -1;
        for(GamePieceGroup enemy:gamePieces){
            for(moveNode move: moveGrid){
                if((int)enemy.getPiece().getLocation().x == (int)move.location.x && (int)enemy.getPiece().getLocation().y == (int)move.location.y){
                    if(target == null){
                        target = enemy;
                        targetDist = move.distance;
                    }
                    else if(move.distance < targetDist || targetDist == -1){
                        target = enemy;
                        targetDist = move.distance;
                    }
                    //if equidistant, select weak target
                    else if(move.distance == targetDist){
                        target = selectWeakest(target, enemy);
                    }
                }
            }
        }
        if(target != null) {
            System.out.printf("Current Target found: X: %f, Y: %f, distance: %d\n\n\n", target.getPiece().getLocation().x, target.getPiece().getLocation().y, targetDist);
        }
        return target;
    }

    private GamePieceGroup selectWeakest(GamePieceGroup enemy1, GamePieceGroup enemy2){
        if(calculateDamage(enemy1) >= enemy1.getHealth()){
            return enemy1;
        }
        if(calculateDamage(enemy2) >= enemy2.getHealth()){
            return enemy2;
        }
        int weaponCompare = compareWeapons(enemy1.weapon, enemy2.weapon);
        if(weaponCompare == 1){
            return enemy1;
        }
        if(weaponCompare == -1){
            return enemy2;
        }
        else{
            return enemy1;
        }

    }

    private int compareWeapons(GamePieceGroup.Weapon yourWep, GamePieceGroup.Weapon theirWep){
        if(yourWep != theirWep) {
            if (yourWep == GamePieceGroup.Weapon.SWORD) {
                if (theirWep == GamePieceGroup.Weapon.BOW) {
                    return 1;
                }
                if (theirWep == GamePieceGroup.Weapon.SPEAR) {
                    return -1;
                }
            }
            if (yourWep == GamePieceGroup.Weapon.BOW) {
                if (theirWep == GamePieceGroup.Weapon.SPEAR) {
                    return 1;
                }
                if (theirWep == GamePieceGroup.Weapon.SWORD) {
                    return -1;
                }
            }
            if (yourWep == GamePieceGroup.Weapon.SPEAR) {
                if (theirWep == GamePieceGroup.Weapon.SWORD) {
                    return 1;
                }
                if (theirWep == GamePieceGroup.Weapon.BOW) {
                    return -1;
                }
            }
        }
        //same weapons
        else{
            return 0;
        }
        return 0;
    }



    public int calculateDamage(GamePieceGroup target){
        int result = target.might;
        int weaponCompare = compareWeapons(weapon, target.weapon);
        if(weaponCompare == 1){
            result += 2;
        }
        else if(weaponCompare == -1){
            result -= 2;
        }
        //no negative damage
        if(result < 0){ result = 0;}
        return result;
    }


    public void wakeUp(){
        stateMachine.changeState(PieceState.SCAN);
    }

    public void nextPiece(){
        for(int i = 0; i < aiGamePieces.size; i++){
            if(this == aiGamePieces.get(i) && i + 1 < aiGamePieces.size){
                aiGamePieces.get(i+1).wakeUp();
                break;
            }
            if(this == aiGamePieces.get(i) && i + 1 == aiGamePieces.size){
                boardStateMachine.changeState(PlayState.PLAYER_TURN);
                //change to player state in chessboard
            }
        }
    }

    public void sleep(){
        stateMachine.changeState(PieceState.SLEEP);
    }

}
