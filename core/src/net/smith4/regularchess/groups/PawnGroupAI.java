package net.smith4.regularchess.groups;

import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;

import net.smith4.regularchess.ai.PlayState;
import net.smith4.regularchess.sprites.Board;
import net.smith4.regularchess.sprites.AiGamePiece;
import net.smith4.regularchess.states.ChessboardState;

/**
 * Created by Warren on 10/26/2017.
 */

public class PawnGroupAI extends AiGamePieceGroup{




    public PawnGroupAI(Board board, Array<GamePieceGroup> gamePieces, Array<AiGamePieceGroup> aiGamePieces, StateMachine<ChessboardState, PlayState> boardStateMachine, int row , int col) {
        super(board, gamePieces, aiGamePieces, boardStateMachine);
        piece = new AiGamePiece(board, "PawnAI.png", row, col);
        setTouchable(Touchable.disabled);
        weapon = GamePieceGroup.Weapon.SWORD;
        health = 10;
        might = 3;
        range = 1;
        addActor(piece);
    }


    @Override
    public void generateAttackMap(Vector3 location) {
        attackGrid.clear();
        float pieceRow = piece.getLocation().x;
        float pieceCol = piece.getLocation().y;
        //range of one square away in any direction
        float range = (float)(Math.sqrt((pieceRow - pieceRow + 1) + (pieceCol - pieceCol + 1)));
        //find piece within range
        for(GamePieceGroup enemy: gamePieces){
            float enemyRow = enemy.getPiece().getLocation().x;
            float enemyCol = enemy.getPiece().getLocation().y;
            float euclidianDist = (float)(Math.sqrt((pieceRow - enemyRow)*(pieceRow - enemyRow) + (pieceCol - enemyCol)*(pieceCol-enemyCol)));
            if(euclidianDist <= range){
                attackGrid.add(enemy.getPiece().getLocation());
            }
        }

    }

    @Override
    public void generateMoveMap(moveNode node, int dist) {
        moveGrid.clear();
        //add a new node
        moveGrid.add(node);

        for(moveNode move:moveGrid) {

            moveNode upNode = new moveNode(new Vector3(move.location.x + 1, move.location.y, 0), move, move.distance + 1);
            if (!checkNodeExists(upNode)) {
                //generateMoveMap(upNode, dist + 1);
                moveGrid.add(upNode);
            }
            moveNode downNode = new moveNode(new Vector3(move.location.x - 1, move.location.y, 0), move, move.distance + 1);
            if (!checkNodeExists(downNode)) {
                //generateMoveMap(downNode, dist + 1);
                moveGrid.add(downNode);
            }
            moveNode leftNode = new moveNode(new Vector3(move.location.x, move.location.y - 1, 0), move, move.distance + 1);
            if (!checkNodeExists(leftNode)) {
                //generateMoveMap(leftNode, dist + 1);
                moveGrid.add(leftNode);
            }
            moveNode rightNode = new moveNode(new Vector3(move.location.x, move.location.y + 1, 0), move, move.distance + 1);
            if (!checkNodeExists(rightNode)) {
                //generateMoveMap(rightNode, dist + 1);
                moveGrid.add(rightNode);
            }
        }
    }



//    private boolean checkNodeExists(moveNode node){
//        //check bounds
//        if(node.location.x >= board.getSize().x || node.location.y >= board.getSize().y || node.location.x < 0 || node.location.y < 0){
//            return true;
//        }
//        //check moveGrid
//        for(int i = 0; i < moveGrid.size; i++){
//            if(node.location.x == moveGrid.get(i).location.x && node.location.y == moveGrid.get(i).location.y){
//                return true;
//            }
//        }
//        //check allies
//        for(int i = 0; i < aiGamePieces.size; i++){
//            float allyX = aiGamePieces.get(i).getPiece().getLocation().x;
//            float allyY = aiGamePieces.get(i).getPiece().getLocation().y;
//            if(node.location.x == allyX && node.location.y == allyY){
//                return true;
//            }
//        }
//        return false;
//    }

}
