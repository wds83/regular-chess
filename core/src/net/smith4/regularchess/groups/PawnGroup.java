package net.smith4.regularchess.groups;



import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import net.smith4.regularchess.sprites.Board;
import net.smith4.regularchess.sprites.GamePiece;
import net.smith4.regularchess.sprites.MoveTile;

/**
 * Created by Warren on 10/15/2017.
 */

public class PawnGroup extends GamePieceGroup{


    public PawnGroup(Board board, Array<GamePieceGroup> gamePieces, Array<AiGamePieceGroup> _aiGamePieces, int row , int col) {
        super(board, gamePieces, _aiGamePieces);
        piece = new GamePiece(board, "pawn.png", row, col);
        weapon = Weapon.SWORD;
        health = 10;
        might = 3;
        range = 1;
        addActor(piece);
        //generateMoves(piece.getLocation());

    }

    @Override
    public void generateMoves(Vector3 location){
//        //check for other pieces
//        boolean blocked1 = false;
//        boolean blocked2 = false;
//        //target gamepiece row and column, respectively
//        int pawnRow = (int)location.x;
//        int pawnCol = (int)location.y;
//        //Check enemy pieces
//        for(AiGamePieceGroup pieceGroup : aiGamePieces){
//            int pieceGroupX = (int)pieceGroup.getPiece().getLocation().x;
//            int pieceGroupY = (int)pieceGroup.getPiece().getLocation().y;
//
//            //Check if there is a piece in front of pawn
//            if(pieceGroupX == pawnRow + 1 && pieceGroupY == pawnCol){
//                blocked1 = true;
//                blocked2 = true;
//            }
//            //Check if there is a piece one space away in front of pawn
//            else if(pieceGroupX == pawnRow + 2 && pieceGroupY == pawnCol){
//                blocked2 = true;
//            }
//            //check if piece to the right (attack)
//            else if(pieceGroupX == pawnRow + 1 && pieceGroupY == pawnCol + 1){
//                moveTiles.add(new MoveTile(board, pawnRow + 1, pawnCol + 1));
//            }
//            //check if piece to the left (attack)
//            else if(pieceGroupX == pawnRow + 1 && pieceGroupY == pawnCol - 1){
//                moveTiles.add(new MoveTile(board, pawnRow + 1, pawnCol - 1));
//            }
//        }
//        //check for friendly pieces blocking the way
//        for(GamePieceGroup pieceGroup : gamePieces){
//            int pieceGroupX = (int)pieceGroup.getPiece().getLocation().x;
//            int pieceGroupY = (int)pieceGroup.getPiece().getLocation().y;
//
//            //Check if there is a piece in front of pawn
//            if(pieceGroupX == pawnRow - 1 && pieceGroupY == pawnCol){
//                blocked1 = true;
//                blocked2 = true;
//            }
//            //Check if there is a piece one space away in front of pawn
//            else if(pieceGroupX == pawnRow - 2 && pieceGroupY == pawnCol){
//                blocked2 = true;
//            }
//
//
//        }
//
//        //check regular movement bounds
//        if( piece.getMoved() && !blocked2 && piece.getLocation().x + 2 < board.getSize().x){
//            moveTiles.add(new MoveTile(board, pawnRow + 2, pawnCol));
//        }
//        if(!blocked1 && piece.getLocation().x + 1 < board.getSize().x){
//            moveTiles.add(new MoveTile(board, pawnRow + 1, pawnCol));
//        }
//
//        for(MoveTile moveTile: moveTiles){
//            moveTile.setVisible(false);
//            addActor(moveTile);
//        }
        if(!moved) {
            Vector3 upNode = new Vector3(piece.getLocation().x + 1, piece.getLocation().y, 0);
            if (checkMovePossible(upNode)) {
                moveTiles.add(new MoveTile(board, (int) upNode.x, (int) upNode.y));
            }
            Vector3 downNode = new Vector3(piece.getLocation().x - 1, piece.getLocation().y, 0);
            if (checkMovePossible(downNode)) {
                moveTiles.add(new MoveTile(board, (int) downNode.x, (int) downNode.y));
            }
            Vector3 leftNode = new Vector3(piece.getLocation().x, piece.getLocation().y - 1, 0);
            if (checkMovePossible(leftNode)) {
                moveTiles.add(new MoveTile(board, (int) leftNode.x, (int) leftNode.y));
            }
            Vector3 rightNode = new Vector3(piece.getLocation().x, piece.getLocation().y + 1, 0);
            if (checkMovePossible(rightNode)) {
                moveTiles.add(new MoveTile(board, (int) rightNode.x, (int) rightNode.y));
            }
        }
        if(!attacked) {

            for (int i = 0; i < aiGamePieces.size; i++) {
                float enemyX = aiGamePieces.get(i).getPiece().getLocation().x;
                float enemyY = aiGamePieces.get(i).getPiece().getLocation().y;

                Vector3 upLeft = new Vector3(piece.getLocation().x + 1, piece.getLocation().y - 1, 0);
                if (piece.getLocation().x + 1 == enemyX && piece.getLocation().y - 1 == enemyY) {
                    MoveTile attackTile = new MoveTile(board, (int) upLeft.x, (int) upLeft.y);
                    attackTile.setMoveType(MoveTile.MoveType.ATTACK);
                    moveTiles.add(attackTile);
                }
                Vector3 downLeft = new Vector3(piece.getLocation().x - 1, piece.getLocation().y - 1, 0);
                if (piece.getLocation().x - 1 == enemyX && piece.getLocation().y - 1 == enemyY) {
                    MoveTile attackTile = new MoveTile(board, (int) downLeft.x, (int) downLeft.y);
                    attackTile.setMoveType(MoveTile.MoveType.ATTACK);
                    moveTiles.add(attackTile);
                }
                Vector3 upRight = new Vector3(piece.getLocation().x + 1, piece.getLocation().y + 1, 0);
                if (piece.getLocation().x + 1 == enemyX && piece.getLocation().y + 1 == enemyY) {
                    MoveTile attackTile = new MoveTile(board, (int) upRight.x, (int) upRight.y);
                    attackTile.setMoveType(MoveTile.MoveType.ATTACK);
                    moveTiles.add(attackTile);
                }
                Vector3 downRight = new Vector3(piece.getLocation().x - 1, piece.getLocation().y + 1, 0);
                if (piece.getLocation().x - 1 == enemyX && piece.getLocation().y + 1 == enemyY) {
                    MoveTile attackTile = new MoveTile(board, (int) downRight.x, (int) downRight.y);
                    attackTile.setMoveType(MoveTile.MoveType.ATTACK);
                    moveTiles.add(attackTile);
                }
            }
        }
        for (MoveTile moveTile : moveTiles) {
            moveTile.setVisible(false);
            addActor(moveTile);
        }

    }



}
