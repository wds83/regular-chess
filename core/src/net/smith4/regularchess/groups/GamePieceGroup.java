package net.smith4.regularchess.groups;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;


import net.smith4.regularchess.sprites.AiGamePiece;
import net.smith4.regularchess.sprites.Board;
import net.smith4.regularchess.sprites.GamePiece;
import net.smith4.regularchess.sprites.MoveTile;

/**
 * Created by Warren on 10/23/2017.
 */


public abstract class GamePieceGroup extends Group {
    protected Array<MoveTile> moveTiles;
    protected Board board;
    protected GamePiece piece;
    protected Array<GamePieceGroup> gamePieces;
    protected Array<AiGamePieceGroup> aiGamePieces;
    protected boolean moved;
    protected boolean attacked;
    protected boolean movesGenerated = false;

    public enum Weapon {
        SWORD,
        BOW,
        SPEAR;
    }
    protected Weapon weapon;
    protected int health;
    protected int might;
    protected int range;
    protected boolean isDead = false;

    public GamePieceGroup(Board _board, Array<GamePieceGroup> _gamePieces, Array<AiGamePieceGroup> _aiGamePieces){
        super();
        board = _board;
        gamePieces = _gamePieces;
        aiGamePieces = _aiGamePieces;
        moveTiles = new Array<MoveTile>();
        moved = false;
        attacked = false;

    }

    @Override
    public void act(float delta) {
        //If a piece is clicked and is waiting for an action
        if(piece.getClicked() && piece.getAct()){
            toFront(); // move piece to front
            if(!movesGenerated){
                generateMoves(piece.getLocation());
                movesGenerated = true;
            }
            showMoves();
            piece.setAct(false);
        //If a piece isn't clicked and is waiting for an action, hide
        }else if(!piece.getClicked() && piece.getAct()){
            hideMoves();
            //destroyMoves();
            piece.setAct(false);
        }
        //if a tile is clicked, move piece
        for(int i = 0; i < moveTiles.size; i++){
            if(moveTiles.get(i).getClicked() && moveTiles.get(i).getMoveType() == MoveTile.MoveType.MOVE){
                moveTiles.get(i).setClicked(false);
                makeMove(moveTiles.get(i));
            }
            //if attack tile is clicked
            else if(moveTiles.get(i).getClicked() && moveTiles.get(i).getMoveType() == MoveTile.MoveType.ATTACK){
                //locate enemy it was over
                moveTiles.get(i).setClicked(false);
                for(int j = 0; j < aiGamePieces.size; j++){
                    if(aiGamePieces.get(j).getPiece().getLocation().x == moveTiles.get(i).getLocation().x &&
                            aiGamePieces.get(j).getPiece().getLocation().y == moveTiles.get(i).getLocation().y){
                        //deal damage to that enemy
                        System.out.printf("about to do %d damage\n", calculateDamage(aiGamePieces.get(j)));
                        makeAttack(aiGamePieces.get(j));
                        break;

                    }
                }
            }
        }
    }

    public void dispose(){
        for(MoveTile mt: moveTiles){
            mt.dispose();
        }
        piece.dispose();
    }

    public void setMoved(boolean state){moved = state;}
    public void setAttacked(boolean state){attacked = state;}

    public void showMoves(){
        for(int i = 0; i < gamePieces.size; i++){
            gamePieces.get(i).hideMoves();
            if(gamePieces.get(i).getPiece() != piece){
                gamePieces.get(i).getPiece().setClicked(false);
            }

        }

        for(MoveTile moveTile:moveTiles){
            moveTile.setVisible(true);
        }
    }

    protected abstract void generateMoves(Vector3 location);

    protected boolean checkMovePossible(Vector3 move){
        //check bounds
        if(move.x >= board.getSize().x || move.y >= board.getSize().y || move.x < 0 || move.y < 0){
            return false;
        }

        //check allies
        for(int i = 0; i < gamePieces.size; i++){
            float enemyX = gamePieces.get(i).getPiece().getLocation().x;
            float enemyY = gamePieces.get(i).getPiece().getLocation().y;
            if(move.x == enemyX && move.y == enemyY){
                return false;
            }
        }
        //check enemies
        for(int i = 0; i < aiGamePieces.size; i++){
            float allyX = aiGamePieces.get(i).getPiece().getLocation().x;
            float allyY = aiGamePieces.get(i).getPiece().getLocation().y;
            if(move.x == allyX && move.y == allyY){
                return false;
            }
        }
        return true;
    }

    protected void destroyMoves(){

        for (MoveTile moveTile : moveTiles) {
            removeActor(moveTile);
            childrenChanged();
        }
        moveTiles.clear();
    }

    protected void hideMoves(){
        //piece.setClicked(false);
        for(MoveTile moveTile:moveTiles){
            moveTile.setVisible(false);
        }
    }

    protected void makeMove(MoveTile moveTile){
        //Remove piece moved over
//        for(int i = 0; i < aiGamePieces.size; i++){
//            if(aiGamePieces.get(i).getPiece().getLocation().x == moveTile.getLocation().x
//                    && aiGamePieces.get(i).getPiece().getLocation().y == moveTile.getLocation().y){
//                if(!aiGamePieces.get(i).checkDead()){
//                    aiGamePieces.get(i).takeDamage(calculateDamage(aiGamePieces.get(i)));
//                }
//                if(aiGamePieces.get(i).checkDead()){
//                    aiGamePieces.get(i).remove();
//                }
//
//            }
//        }
        piece.setMoved();//set first move to false for pawns
        piece.updatePosition(moveTile.getPosition());
        piece.updateLocation(moveTile.getLocation());
        piece.setClicked(false);
        //destroy all moves for all pieces
        for(int i = 0; i < gamePieces.size; i++){
            gamePieces.get(i).destroyMoves();
            gamePieces.get(i).movesGenerated = false;
        }
        moved = true;

    }

    protected void makeAttack(AiGamePieceGroup target){
        target.takeDamage(calculateDamage(target));
        //hideMoves();
        destroyMoves();
        moved = true;
        attacked = true;
        this.setTouchable(Touchable.disabled);
    }

    public GamePiece getPiece(){
        return piece;
    }


    public int getHealth(){return health;}

    public void takeDamage(int value){
        health -= value;
        if(health <= 0){
            isDead = true;
            removePiece(this);
        }
    }
    public void removePiece(GamePieceGroup piece){
        for(int i = 0; i < gamePieces.size; i++){
            if(gamePieces.get(i).getPiece().getLocation().x == piece.getPiece().getLocation().x && gamePieces.get(i).getPiece().getLocation().y == piece.getPiece().getLocation().y){
                gamePieces.removeIndex(i);
                this.getParent().removeActor(this);
            }
        }
    }

    public int getMight(){return might;}

    public boolean checkDead(){ return isDead;}

    public int calculateDamage(AiGamePieceGroup target){
        int result = target.might;
        if(target.weapon == Weapon.SWORD){
            //damage remains the same
        }
        //If target has a bow, add 2 damage
        if(target.weapon == Weapon.BOW){
            result += 2;
        }
        //If target has a spear, subtract 2 damage
        if(target.weapon == Weapon.SPEAR){
            result -= 2;
        }
        //add priority if damage would kill target
        if(result >= target.health){
            result += 5;
        }
        return result;
    }


}
