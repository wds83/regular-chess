package net.smith4.regularchess.ai;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;

import net.smith4.regularchess.states.ChessboardState;

/**
 * Created by Warren on 10/29/2017.
 */

public enum PlayState implements State<ChessboardState> {

    PLAYER_TURN(){
        @Override
        public void update(ChessboardState entity){

        }
        @Override
        public void enter(ChessboardState entity){
            entity.initPlayerTurn();
        }
        @Override
        public void exit(ChessboardState entity) {
            entity.exitPlayerTurn();
        }
    },
    AI_TURN(){
        @Override
        public void update(ChessboardState entity){

        }
        @Override
        public void enter(ChessboardState entity) {
            entity.initAiTurn();

        }
        @Override
        public void exit(ChessboardState entity) {

        }
    };


    @Override
    public boolean onMessage(ChessboardState entity, Telegram telegram) {
        return false;
    }
}
