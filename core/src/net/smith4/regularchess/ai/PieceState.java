package net.smith4.regularchess.ai;


import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;

import com.badlogic.gdx.utils.Timer;

import net.smith4.regularchess.groups.AiGamePieceGroup;
import net.smith4.regularchess.groups.GamePieceGroup;

/**
 * Created by Warren on 11/2/2017.
 */

public enum PieceState implements State<AiGamePieceGroup> {
    SLEEP(){
        @Override
        public void update(AiGamePieceGroup entity){
        }
        @Override
        public void enter(AiGamePieceGroup entity){
            System.out.println("Zzzzzz...");
            entity.hideMove("attackActor");
            final AiGamePieceGroup currentEntity = entity;

            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    currentEntity.nextPiece();
                }
            }, 0.25f);;


        }
        @Override
        public void exit(AiGamePieceGroup entity) {
        }
    },
    SCAN(){
        @Override
        public void update(AiGamePieceGroup entity){
        }
        @Override
        public void enter(AiGamePieceGroup entity) {
            //find target
            GamePieceGroup target = entity.findTarget();
            entity.setTarget(target);
            //try to attack first
            if(target == null){
                System.out.println("No targets, sleeping...");
                entity.sleep();
            }
            else if(entity.canAttack(target)){
                entity.attack();
            //then try to move
            }else{
                entity.move();
            }

        }
        @Override
        public void exit(AiGamePieceGroup entity) {

        }
    },
    MOVE(){
        @Override
        public void update(AiGamePieceGroup entity){


        }
        @Override
        public void enter(AiGamePieceGroup entity) {
            //find move toward target
            entity.backtraceMove(entity.getTarget());
            entity.attack();



        }
        @Override
        public void exit(AiGamePieceGroup entity) {

        }
    },
    ATTACK(){
        @Override
        public void update(AiGamePieceGroup entity){

        }
        @Override
        public void enter(AiGamePieceGroup entity) {
            //deal damage to target
            //cannot move after attack, send to sleep state
//            entity.generateAttackMap(entity.getPiece().getLocation());
//            if(entity.canAttack(entity.getTarget())){
//                entity.displayAttack();
//                entity.dealDamage(entity.getTarget());
//            }
//            final AiGamePieceGroup currentEntity = entity;
//
//            Timer.schedule(new Timer.Task() {
//                @Override
//                public void run() {
//                    currentEntity.sleep();
//                }
//            }, 0.25f);
            entity.makeAttack();






        }
        @Override
        public void exit(AiGamePieceGroup entity) {


        }
    };



    @Override
    public boolean onMessage(AiGamePieceGroup entity, Telegram telegram) {
        return false;
    }
}
