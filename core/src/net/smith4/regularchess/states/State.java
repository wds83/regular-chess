package net.smith4.regularchess.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.smith4.regularchess.RegularChessMain;

/**
 * Created by Warren on 10/14/2017.
 */

public abstract class State {
    protected OrthographicCamera cam;
    protected Vector3 mouse;
    protected StateManager statemgr;
    protected Viewport viewport;

    protected State(StateManager statemgr){
        this.statemgr = statemgr;
        cam = new OrthographicCamera();
        viewport = new StretchViewport(RegularChessMain.WIDTH, RegularChessMain.HEIGHT, cam);
        mouse = new Vector3();
    }

    protected abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();
    public abstract void resize(int width, int height);
}
