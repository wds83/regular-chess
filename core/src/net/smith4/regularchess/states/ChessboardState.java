package net.smith4.regularchess.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import net.smith4.regularchess.RegularChessMain;
import net.smith4.regularchess.ai.PlayState;
import net.smith4.regularchess.groups.AiGamePieceGroup;
import net.smith4.regularchess.groups.GamePieceGroup;
import net.smith4.regularchess.groups.PawnGroup;
import net.smith4.regularchess.groups.PawnGroupAI;
import net.smith4.regularchess.sprites.Board;


/**
 * Created by Warren on 10/14/2017.
 */

public class ChessboardState extends State {

    private Texture bg;
    private Board board;
    private Stage stage;
    private Array<GamePieceGroup> gamePieces;
    private Array<AiGamePieceGroup> aiGamePieces;
    private ImageButton turnButton;
    private StateMachine<ChessboardState, PlayState> stateMachine;

    public ChessboardState(StateManager statemgr) {
        super(statemgr);
        viewport.apply();
        cam.setToOrtho(false, RegularChessMain.WIDTH, RegularChessMain.HEIGHT);
        bg = new Texture("bg.png");
        board = new Board(RegularChessMain.WIDTH/2, RegularChessMain.HEIGHT/2, 8, 8);
        gamePieces = new Array<GamePieceGroup>();
        aiGamePieces = new Array<AiGamePieceGroup>();
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        stateMachine = new DefaultStateMachine<ChessboardState, PlayState>(this, PlayState.PLAYER_TURN);

        Drawable turnButtonTexture = new TextureRegionDrawable(new TextureRegion(new Texture("turnbutton.png")));
        turnButton = new ImageButton(turnButtonTexture);
        turnButton.setPosition(300, 50);
        //upon turnbutton click, stage is no longer touchable by player, including turnbutton
        turnButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                stateMachine.changeState(PlayState.AI_TURN);
                System.out.println("Button clicked");
                //stateMachine.update();

            }
        });

        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 0, 2));
        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 4, 2));
        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 3, 3));
        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 7, 7));
        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 0, 0));
        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 0, 7));
        gamePieces.add(new PawnGroup(board, gamePieces, aiGamePieces, 7, 0));
        aiGamePieces.add(new PawnGroupAI(board, gamePieces, aiGamePieces, stateMachine, 5,3));
        aiGamePieces.add(new PawnGroupAI(board, gamePieces, aiGamePieces, stateMachine, 1,2));
        aiGamePieces.add(new PawnGroupAI(board, gamePieces, aiGamePieces, stateMachine, 3,5));
        for(GamePieceGroup piece:gamePieces){
            stage.addActor(piece);
        }
        for(AiGamePieceGroup piece:aiGamePieces){
            stage.addActor(piece);
        }
        stage.addActor(turnButton);
    }

    @Override
    protected void handleInput() {

    }

    @Override
    public void update(float dt) {
        stage.act(dt);


    }

    @Override
    public void render(SpriteBatch sb) {
        cam.update();
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(bg, 0, 0);
        sb.draw(board.getBoardTexture(), board.getPosition().x, board.getPosition().y);
        sb.end();
        update(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void dispose() {
        bg.dispose();
        board.dispose();
        for(GamePieceGroup pieceGroup:gamePieces){
            pieceGroup.clear();
            pieceGroup.dispose();
        }
        stage.dispose();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        stage.setViewport(viewport);

    }



    public void exitPlayerTurn(){
        for(GamePieceGroup currentpiece:gamePieces){
            currentpiece.getPiece().setClicked(false);
            currentpiece.getPiece().setAct(true);
            currentpiece.setTouchable(Touchable.disabled);


        }
        turnButton.setTouchable(Touchable.disabled);
    }

    public void initPlayerTurn(){
        for(GamePieceGroup currentpiece:gamePieces){
            currentpiece.setTouchable(Touchable.enabled);
            currentpiece.setMoved(false);
            currentpiece.setAttacked(false);

        }
        turnButton.setTouchable(Touchable.enabled);
    }


    public void initAiTurn(){
        if(aiGamePieces.size > 0) {
            aiGamePieces.get(0).wakeUp();
        }
        else{
            initPlayerTurn();
        }


        //stateMachine.changeState(PlayState.PLAYER_TURN);
    }

}
