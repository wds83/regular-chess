package net.smith4.regularchess.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AddAction;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.hide;

/**
 * Created by Warren on 10/15/2017.
 */

public class MoveTile extends Actor{
    private Texture texture;
    private Vector3 position;
    private boolean clicked;
    private Vector3 location;
    public enum MoveType{
        MOVE,
        ATTACK;
    }
    private MoveType moveType;

    public MoveTile(Board board, int row, int col) {
        texture = new Texture("movement.png");
        moveType = MoveType.MOVE;
        setName("movetile");
        clicked = false;
        location = new Vector3(row, col, 0);
        Vector3 locationVector = board.getLocation(row, col);
        position = new Vector3(locationVector.x, locationVector.y, 0);
        setBounds(position.x, position.y, texture.getWidth(), texture.getHeight());
        addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
                toggleClicked();
                System.out.printf("MoveTile Clicked = %b\n", clicked);
                return true;
            }
        });

    }

    public void setMoveType(MoveType mt){
        if(mt == MoveType.MOVE){
            texture = new Texture("movement.png");
        }else if(mt == MoveType.ATTACK){
            texture = new Texture("attack.png");
        }
        moveType = mt;
    }

    public MoveType getMoveType(){return moveType;}

    public Vector3 getPosition(){
        return position;
    }

    public Vector3 getLocation(){return location;}

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, position.x, position.y);
    }

    @Override
    public void act(float delta) {

        //super.act(delta);
    }

    @Override
    public boolean remove() {
        clear();
        texture.dispose();
        return super.remove();
    }

    public void toggleClicked(){
        if(clicked == false){
            clicked = true;
        }else{
            clicked = false;
        }
    }

    public void setClicked(boolean state){
        clicked = state;
    }

    public boolean getClicked(){return clicked;}

    public void dispose(){
        texture.dispose();
    }


//    @Override
//    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
//        if(clicked == false){
//            clicked = true;
//            System.out.println("moveTile updated: True");
//        }else{
//            clicked = false;
//            System.out.println("MoveTile updated: False");
//
//        }
//        return true;
//    }


}
