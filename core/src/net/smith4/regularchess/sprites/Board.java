package net.smith4.regularchess.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;


import net.smith4.regularchess.RegularChessMain;

import static com.badlogic.gdx.graphics.Pixmap.Format.RGB565;

/**
 * Created by Warren on 10/14/2017.
 */

public class Board {
    private static final int TILE_SIZE = 55;
    private Array<Texture> board;
    private FrameBuffer fb;
    private Vector3 position;
    private int rows;
    private int cols;
    private float width;
    private float height;
    private SpriteBatch sb; //new batch for board gen texture
    private Texture boardTexture;


    public Board(int x, int y, int _rows, int _columns) {
        rows = _rows;
        cols = _columns;

        position = new Vector3(x-((cols*TILE_SIZE)/2), (y -((rows*TILE_SIZE)/2)), 0);

        width = cols * TILE_SIZE;
        height = rows * TILE_SIZE;
        board = new Array<Texture>();

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(i % 2 == 0){
                    if(j % 2 == 0){
                        board.add(new Texture("whitetile.png"));
                    }
                    else{
                        board.add(new Texture("blacktile.png"));
                    }
                }
                else{
                    if(j % 2 == 0){
                        board.add(new Texture("blacktile.png"));
                    }
                    else{
                        board.add(new Texture("whitetile.png"));
                    }
                }
            }
        }
        try{
            fb = new FrameBuffer(RGB565, cols*TILE_SIZE, rows*TILE_SIZE, false);
        }catch (Throwable t){
            Gdx.app.error("Board Constructor", "Invald Frame Buffer size");
            t.getCause().printStackTrace();
            Gdx.app.exit();
        }

        sb = new SpriteBatch();
        boardTexture = generateTexture();



    }

    public Vector3 getSize(){return new Vector3(rows, cols, 0);}

    public Vector3 getPosition(){
        return position;
    }

    //Get the coordinate of the origin of a tile on the board
    public Vector3 getLocation(int row, int column){
        float xPos = position.x + (column * TILE_SIZE);
        float yPos = position.y + (row * TILE_SIZE);

        return new Vector3(xPos, yPos, 0);

    }



    public Texture generateTexture(){
        fb.begin();

        sb.getProjectionMatrix().setToOrtho2D(position.x,position.y,fb.getWidth(),fb.getHeight());
        sb.begin();
        float posX = position.x;
        float posY = position.y;
        int current = 0;
        for(int i = 1; i <= rows; i++){
            for(int j = 0; j < cols; j++){
                sb.draw(board.get(current), posX, posY);
                current++;
                posX += TILE_SIZE;
            }
            posX = position.x;
            posY += TILE_SIZE;
        }
        sb.end();

        Texture newTexture = fb.getColorBufferTexture();
        sb.getProjectionMatrix().setToOrtho2D(0, 0, RegularChessMain.WIDTH,RegularChessMain.HEIGHT);

        fb.end();

        return newTexture;
    }

    public Texture getBoardTexture(){
        return boardTexture;
    }

    public void dispose(){
        for(Texture texture : board){
            texture.dispose();
        }
        boardTexture.dispose();
        sb.dispose();
        fb.dispose();
    }


}
