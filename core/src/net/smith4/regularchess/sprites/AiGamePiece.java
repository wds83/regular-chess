package net.smith4.regularchess.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;



/**
 * Created by Warren on 10/29/2017.
 */

/**
 * AiGamePiece is very similar to GamePiece but it does not include listeners and accompanying variables
 */
public class AiGamePiece extends Actor {
    private Vector3 position;
    private boolean clicked;
    private Vector3 location;
    private Texture texture;
    private boolean firstMove; //for pawns, tracks first move

    public AiGamePiece(Board board, String filename, int row, int col){
        super();
        clicked = false;
        firstMove = true;

        texture = new Texture(filename);
        location = new Vector3(row, col , 0);
        position = board.getLocation(row,col);
        setBounds(position.x, position.y, texture.getWidth(), texture.getHeight());

    }

    public Vector3 getPosition(){
        return position;
    }

    public void updatePosition(Vector3 newPos){
        position = newPos;
        //setPosition(newPos.x, newPos.y);
        setBounds(position.x, position.y, texture.getWidth(), texture.getHeight());
    }
    public void updateLocation(Vector3 newLoc){
        location = newLoc;
    }

    public Vector3 getLocation(){
        return location;
    }

    //For Pawns, tracks first move
    public void setMoved(){
        firstMove = false;
    }

    public boolean getMoved(){return firstMove;}

    @Override
    public boolean remove() {
        clear();
        texture.dispose();
        return super.remove();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, position.x, position.y);

    }
}
