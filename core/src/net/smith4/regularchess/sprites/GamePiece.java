package net.smith4.regularchess.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

/**
 * Created by Warren on 10/23/2017.
 */

public class GamePiece extends Actor {
    private Vector3 position;
    private boolean clicked;
    private Vector3 location;
    private Texture texture;
    private boolean firstMove; //for pawns, tracks first move
    private boolean act; //Determines if piece requires an action


    public GamePiece(Board board, String filename, int row, int col){
        super();
        clicked = false;
        firstMove = true;
        act = false;

        texture = new Texture(filename);
        location = new Vector3(row, col , 0);
        position = board.getLocation(row,col);
        setBounds(position.x, position.y, texture.getWidth(), texture.getHeight());

        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(!clicked){
                    System.out.println("Piece clicked : true");
                    clicked = true;
                    act = true;
                }else if(clicked){
                    System.out.println("Piece clicked : false");
                    clicked = false;
                    act = true;
                }
                return true;
            }
        });


    }
    public Vector3 getPosition(){
        return position;
    }

    public void updatePosition(Vector3 newPos){
        position = newPos;
        //setPosition(newPos.x, newPos.y);
        setBounds(position.x, position.y, texture.getWidth(), texture.getHeight());
    }
    public void updateLocation(Vector3 newLoc){
        location = newLoc;
    }

    public Vector3 getLocation(){
        return location;
    }

    public void setClicked(boolean state){
        clicked = state;
    }

    public boolean getClicked(){return clicked;}

    public void toggleClicked(){
        if(clicked == false){
            clicked = true;
        }else{
            clicked = false;
        }
    }

    //For Pawns, tracks first move
    public void setMoved(){
        firstMove = false;
    }

    public boolean getMoved(){return firstMove;}

    public boolean getAct(){return act;}

    public void setAct(boolean state){act = state;}

    public void dispose(){
        texture.dispose();
    }

    @Override
    public boolean remove() {
        clear();
        texture.dispose();
        return super.remove();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, position.x, position.y);

    }

}
