package net.smith4.regularchess;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.smith4.regularchess.states.ChessboardState;
import net.smith4.regularchess.states.StateManager;

public class RegularChessMain extends ApplicationAdapter {
	public static final int WIDTH = 480;
	public static final int HEIGHT = 800;




	public static final String TITLE = "Regular Chess";
	private StateManager statemgr;
	private SpriteBatch batch;


	@Override
	public void create () {
		batch = new SpriteBatch();
		statemgr = new StateManager();
		Gdx.gl.glClearColor(1, 0, 0, 1);
		statemgr.push(new ChessboardState(statemgr));
	}

	@Override
	public void render () {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		statemgr.update(Gdx.graphics.getDeltaTime());
		statemgr.render(batch);

	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}

	@Override
	public void resize(int width, int height) {
		statemgr.peek().resize(width, height);
	}


}
